const path = require('path');
const webpack = require('webpack');

/*
 * We've enabled MiniCssExtractPlugin for you. This allows your app to
 * use css modules that will be moved into a separate CSS file instead of inside
 * one of your module entries!
 *
 * https://github.com/webpack-contrib/mini-css-extract-plugin
 *
 */

const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin')

/*
 * We've enabled HtmlWebpackPlugin for you! This generates a html
 * page for you when you compile webpack, which will make you start
 * developing and prototyping faster.
 *
 * https://github.com/jantimon/html-webpack-plugin
 *
 */

module.exports = {
  mode: 'production',
  entry: { 
    home: './src/index.js',
    worldclock: './src/worldclock.js'
  },
  watch: false,
  output: {
    path: path.resolve(__dirname, './dist')
    

  },

  plugins: [
    new webpack.ProgressPlugin(),
    new MiniCssExtractPlugin({ filename:'main.[contenthash].css' }),
    new HtmlWebpackPlugin({
              template: path.resolve(__dirname, "src/template", "index.html")
            }),
    new HtmlWebpackPlugin({      
              filename: 'worldclock.html',
              template: path.resolve(__dirname, "src/template", "worldclock.html")
            }),
    new HtmlWebpackPlugin({      
              filename: 'convert.html',
              template: path.resolve(__dirname, "src/template", "convert.html")
            })   

  ],

  module: {
    rules: [{
      test: /\.m?js$/,
      exclude: /node_modules/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: [
            ["@babel/preset-env", {
              "useBuiltIns": "usage",
              "loose": true,
              "modules": "auto",
              "corejs": 3
            }]
            
          ],
          plugins: [
            '@babel/plugin-proposal-class-properties',
            "syntax-dynamic-import",
            "transform-class-properties"
          ]
          

        }
      }
    }, {
      test: /.css$/,

      use: [{
        loader: MiniCssExtractPlugin.loader
      }, {
        loader: "css-loader",

        options: {
          sourceMap: true
        }
      }]
    }]
  },

  
}