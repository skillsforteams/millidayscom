'use strict';
import SiTime from './SiTime';

class MillidaysTime {
  
  /*
  class attributes do not exists in es 6
  beats;
  millidays;
  centidays;
  decidays;
  beatsOfDay;  
  microDaysOfDay; */

  constructor (microDaysOfDay) {
    this.microDaysOfDay = microDaysOfDay;
    this.calculateMillidays();        
  }

  static createFromSiTime(siTime) {
    let microDaysOfDay = Math.floor(siTime.getMilliSecondsOfDay() / 864 * 10);
    return new MillidaysTime(microDaysOfDay);
  } 

  static createFromMillidays(millidays) {
    let microDaysOfDay = millidays*1000;
    return new MillidaysTime(microDaysOfDay);
  } 
  getMicroSecondsOfDay() {
    let microSecondsOfDay = this.microDaysOfDay * 86.4;
    return microSecondsOfDay;
  } 
  calculateMillidays() {
    this.beatsOfDay = Math.floor(this.microDaysOfDay / 10);
    this.beats = this.beatsOfDay % 100;
    this.millidays = Math.floor(this.beatsOfDay / 100);
    this.centidays = Math.floor(this.millidays / 10);
    this.decidays = Math.floor(this.millidays / 100);
  }

  getMillidays() {    
    return this.millidays;
  }
  getBeats() {
    return this.beats;
  }
  getDecidays() {
    return this.decidays;
  }
  getCentidays() {
    return this.centidays;
  }
  

 }
 export default MillidaysTime;