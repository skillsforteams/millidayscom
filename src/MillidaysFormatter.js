'use strict';
import MillidaysTime from './MillidaysTime';

class MillidaysFormatter {

    construct() {

    }
    static getFormatted(millidaysTime) {
        let millidays = millidaysTime.getMillidays();
        let beats =  millidaysTime.getBeats();
        let decidays = millidaysTime.getDecidays();
        
        if ( beats < 10 ) {
            beats = "0" + beats;
        }
    
        if(decidays < 1) {
            decidays = 0;
        }
        let middlepart = millidays - (decidays*100);        
        if(middlepart<10) {
            middlepart = "0" + middlepart;
        }
        
    

        return decidays+"."+middlepart +":" + beats;

    }
}

export default MillidaysFormatter;