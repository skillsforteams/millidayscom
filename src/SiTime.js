'use strict';
class SiTime { 

    /* class members do not need to be declared in es6 
    milliseconds; 
    millisecondsOfDay;
    */

    constructor(hours, minutes, seconds) {
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;        
    }
    

    getMilliSecondsOfDay( ){
        this.millisecondsOfDay = ((this.hours * 60 + this.minutes)*60 + this.seconds) * 1000; 
        return this.millisecondsOfDay;
    }
    
}
export default SiTime;