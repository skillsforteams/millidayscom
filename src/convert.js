import MillidaysTime from './MillidaysTime';
import MillidaysFormatter from './MillidaysFormatter';
import SiTime from './SiTime';
import Timezones from './Timezones';



console.log("convert.js executed");

let convertTime = function(e) {
    let h = parseInt(document.getElementById('hours').value);
    let m = parseInt(document.getElementById('minutes').value);    
    let s = parseInt(document.getElementById('seconds').value); 

    let timezoneOffset = parseInt(document.getElementById('timeZoneSelect').value);
    let timezoneString = "UTC";
    let today = new Date();
    if(timezoneOffset > 0) {
        timezoneString= "UTC+"+timezoneOffset;
    } else if (timezoneOffset <0) {
    timezoneString= "UTC-"+(timezoneOffset*-1);
    } 
        
    
    let utcDate = new Date(Date.parse(today.getDay()+' '+today.getMonth()+' '+today.getFullYear()+' '+h+':'+m+':'+s+' '+timezoneString));
    let siTime = new SiTime(utcDate.getUTCHours(), utcDate.getUTCMinutes(), utcDate.getUTCSeconds());

    let millidaysTime = MillidaysTime.createFromSiTime(siTime);
    let timerDisplay = MillidaysFormatter.getFormatted(millidaysTime);
    document.getElementById("TranslatedMillidayClock").innerText = timerDisplay;        
};



const timeConvertButton = document.getElementById("timeConvertButton");
timeConvertButton.addEventListener("click", convertTime);

const timezoneSelect = document.getElementById("timeZoneSelect");

Timezones.forEach(function(timezone) {
    let opt = document.createElement('option'); 
    opt.value=timezone.offset;
    opt.appendChild(document.createTextNode(timezone.text));
    timezoneSelect.appendChild(opt);    
    }
);