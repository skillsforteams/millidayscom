import MillidaysTime from './MillidaysTime';
import MillidaysFormatter from './MillidaysFormatter';
import SiTime from './SiTime';
import Timezones from './Timezones';


function showTimeZone(Timezone) {
    let time = new Date();
    let timerDisplay; 
    let timezoneTime = new Date();
   
    timezoneTime.setHours(time.getHours() + Timezone.offset);   
    let minutes = timezoneTime.getMinutes(); 
    
    if (minutes < 10 ) {
        minutes = "0"+minutes;
    }
    timerDisplay = (timezoneTime.getHours()) +":"+minutes;
    //timerDisplay = Timezone.offset;
    let timezonecode = Timezone.value.replace(' ','-'); 
    document.getElementById("LocalClock"+timezonecode).innerText = timerDisplay;
    setTimeout(showTimeZone, 500, Timezone);    
}

// world clock controller 

let startWorldclocks = false; 
if(null != document.getElementById("WorldClocks")) {
    startWorldclocks = true;
}
if(startWorldclocks == true) {
    Timezones.forEach(function(timezone) {
        let timezoneId = timezone.value.replace(' ','-');
            
            let clockHtml = '<i>'+timezone.value+'</i>  '+'  <b id="LocalClock'+timezoneId+'"></b>';        
            let clockHolder = document.createElement("div");
                clockHolder.innerHTML=clockHtml;
            
            document.getElementById("WorldClocks").appendChild(clockHolder);
                
            
            showTimeZone(timezone);
        
    });
}

function showSiTime(){    
    let localDate = new Date();
    let h = localDate.getUTCHours();
    let m = localDate.getUTCMinutes();
    let s = localDate.getUTCSeconds();
   
    if(m<10 ){
        m = "0"+m;
    }
    if(s<10){
        s = "0"+s;
    }
    document.getElementById("SiTimeClock").innerText = h+":"+m+":"+s;    
    setTimeout(showSiTime, 250);    
}
// Si Clock Controller 
let startSiClock = false; 
if(null != document.getElementById("SiTimeClock")) {
    showSiTime();
}

