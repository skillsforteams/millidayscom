import MillidaysTime from './MillidaysTime';
import SiTime from './SiTime';


test('0 is 0', () => {
  let millidaysTime = new MillidaysTime(0);   
  expect(millidaysTime.getMillidays()).toBe(0);    
});

test('00:00:00 is 0', () => {
  let siTime = new SiTime(0,0,0);
  let millidaysTime = MillidaysTime.createFromSiTime(siTime);      
  expect(millidaysTime.getMillidays()).toBe(0);    
  });

test('12:00:00 si to md', () => {
    let siTime = new SiTime(12,0,0);
    let millidaysTime = MillidaysTime.createFromSiTime(siTime);
    let millidays = millidaysTime.getMillidays();    
    expect(millidays).toBe(500);
  });

test('md0.01:00 getMicroSecondsOfDay 864ms', () => {
    let millidaysTime = new MillidaysTime(1000);   
    expect(millidaysTime.getMicroSecondsOfDay()).toBe(86400);    
});  

test('md5.00:00 to si', () => {
    //43200 000 microTimeOfDay     
    let millidaysTime = MillidaysTime.createFromMillidays(500);
    let microSecondsOfDay = millidaysTime.getMicroSecondsOfDay();    
    expect(microSecondsOfDay).toBe(43200000);
});

/**
 * Data provider realized with json object 
 */

const timeConvertDataProvider = [
  [ 6, 0, 0, 250 ],
  [ 12, 6, 0, 504 ]
];
test.each(timeConvertDataProvider)('convert time  %s', (siHours, siMinutes, siSeconds, expectedMillidays) => {
    let siTime = new SiTime(siHours, siMinutes, siSeconds);
    let millidaysTime = MillidaysTime.createFromSiTime(siTime);
    let millidays = millidaysTime.getMillidays();    
    expect(millidays).toBe(expectedMillidays);    
});


/*
 Alternative layout with tables,
 in scenario outline style from 
*/

test.each`
  h    | m    | s    | expected
  ${12}| ${3} | ${0} | ${502}
  ${6} | ${0} | ${0} | ${250}
  ${24}| ${0} | ${0} | ${1000}
  ${2} | ${30}| ${0} | ${104}
`('converts $h:$m:$s to $expected', ({h, m, s, expected}) => {
  let siTime = new SiTime(h, m, s);
  let millidaysTime = MillidaysTime.createFromSiTime(siTime);
  let millidays = millidaysTime.getMillidays();    
  expect(millidays).toBe(expected);  
});


test.each`
  h    | m    | s     | expected
  ${12}| ${3} | ${0}  | ${5}
  ${2} | ${23}| ${0}  | ${0}
  ${2} | ${25}| ${0}  | ${1}
`('checks dcidays for $h:$m:$s to $expected', ({h, m, s, expected}) => {
  let siTime = new SiTime(h, m, s);
  let millidaysTime = MillidaysTime.createFromSiTime(siTime);
  let decidays = millidaysTime.getDecidays();    
  expect(decidays).toBe(expected);  
});


test.each`
  h    | m     | s     | expected
  ${0} | ${14} | ${23} | ${0}
  ${0} | ${14} | ${25} | ${1}
  ${2} | ${30} | ${0} | ${10}
`('checks centidays for $h:$m:$s to $expected', ({h, m, s, expected}) => {
  let siTime = new SiTime(h, m, s);
  let millidaysTime = MillidaysTime.createFromSiTime(siTime);
  let centidays = millidaysTime.getCentidays();    
  expect(centidays).toBe(expected);  
});


  test.skip('06:00:00 is 2:50.00', () => {   
    //@todo realize with data provider
  });  
  test.skip('2:12:00 is 1 00:00', () => {
    //@todo realize with data provider
  });
  test.skip('0:01:26 is 0.01:00', () => {
    //@todo realize with data provider
  });
  