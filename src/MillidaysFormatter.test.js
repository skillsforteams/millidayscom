import MillidaysTime from './MillidaysTime';
import MillidaysFormatter from './MillidaysFormatter';
/*
 Test for the minidays formater.
*/

test.each`
  microdays    |  expectedFormat
  ${500000}       |  ${"5.00:00"}
  ${999990}       |  ${"9.99:99"}
  ${123450}       |  ${"1.23:45"} 
  ${30}       |  ${"0.00:03"}
  ${4010}       |  ${"0.04:01"}
`('converts $h:$m:$s to $expected', ({microdays, expectedFormat}) => {
  let millidaysTime = new MillidaysTime(microdays);
  let millidaysFormatted  = MillidaysFormatter.getFormatted(millidaysTime);
  expect(millidaysFormatted).toBe(expectedFormat);  
});

