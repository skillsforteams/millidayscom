import SiTime from './SiTime';


test('1 second in milliseconds', () => {
    let siTimeObj = new SiTime(0,0,1);

    expect(siTimeObj.getMilliSecondsOfDay()).toBe(1000);    
  });
  test('01:01 in milliseconds', () => {
    let siTimeObj = new SiTime(0,1,1);
    expect(siTimeObj.getMilliSecondsOfDay()).toBe(61000);    
  });
  test('01:01:01 in milliseconds', () => {
    let siTimeObj = new SiTime(1,1,1);
    expect(siTimeObj.getMilliSecondsOfDay()).toBe(3661000);    
  });


