import MillidaysTime from './MillidaysTime';
import MillidaysFormatter from './MillidaysFormatter';
import SiTime from './SiTime';

const showTime = function(){    
    let localDate = new Date();
    let h = localDate.getUTCHours();
    let m = localDate.getUTCMinutes();
    let s = localDate.getUTCSeconds();
   
    let siTime = new SiTime(h,m,s);
    let millidaysTime = MillidaysTime.createFromSiTime(siTime);
    let timerDisplay = MillidaysFormatter.getFormatted(millidaysTime);

    document.getElementById("MillidayClock").innerText = timerDisplay;
    document.getElementById("MillidayClock").textContent = timerDisplay;
    setTimeout(showTime, 50);    
};
window.onload = showTime;
